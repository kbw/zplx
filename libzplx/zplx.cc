#include "zplx.hh"
#include <unistd.h>
#include <zmq.hpp>
#include <vector>

namespace zplx {
	void process(entries_t& entries) {
		std::vector<zmq::pollitem_t> items;
		items.resize(entries.size());

		const int bufsz{ 16*1024 };
		char* buf = new char[bufsz];
		std::unique_ptr<char> ubuf(buf);

		while (true) {
			// build pollitem_ts
			int ninputs{};
			for (size_t i = 0; i != entries.size(); ++i) {
				const entry_t& entry = entries[i];
				zmq::pollitem_t item { entry.sock, entry.fd, entry.ioflags, 0 };
				items[i] = item;
				if (item.events & ZMQ_POLLIN)
					++ninputs;
			}

			// wait
			int nitems = zmq::poll(&items.front(), items.size(), 1);
			if (nitems <= 0)
				break;

			// process events
			ssize_t rbytes{};
			for (size_t i = 0; i != items.size(); ++i)
				if (items[i].revents & ZMQ_POLLIN) {
					if (items[i].fd != -1) {
						rbytes += read(items[i].fd, buf+rbytes, bufsz-rbytes);
						if (rbytes == 0)
							--ninputs;
					}
				}
			ssize_t wbytes{};
			for (size_t i = 0; i != items.size(); ++i)
				if (items[i].revents & ZMQ_POLLOUT) {
					if (items[i].fd != -1) {
						wbytes = write(items[i].fd, buf, rbytes);
					}
				}
			if (ninputs == 0)
				break;
		}
	}
}
