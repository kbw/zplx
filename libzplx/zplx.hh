#pragma once

#include <vector>

namespace zmq { class socket_t; }

namespace zplx {
	struct entry_t {
		entry_t(int fd, short ioflags) :
			fd(fd), sock(nullptr), ioflags(ioflags) {}
		entry_t(zmq::socket_t* sock, short ioflags) :
			fd(-1), sock(sock), ioflags(ioflags) {}

		int fd;
		zmq::socket_t* sock;
		short ioflags;
	};
	typedef std::vector<entry_t> entries_t;

	void process(entries_t& entries);
}
