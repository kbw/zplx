include ../mk/platform.mk
include ../mk/compile.mk

ifeq ($(strip $(PLATFORM)), Darwin)
  LDFLAGS += -dynamiclib
else
  CPPFLAGS += -fPIC
  LDFLAGS += -shared
endif

ifeq ($(strip $(PLATFORM)), Darwin)
  SHLIBNAME := lib$(SHLIB)-$(SHLIB_MAJOR).dylib
  SHLINKNAME:= lib$(SHLIB).dylib
else
  SHLIBNAME := lib$(SHLIB)-$(SHLIB_MAJOR).so
  SHLINKNAME:= lib$(SHLIB).so
endif

.PHONY: install uninstall
all: $(SHLIBNAME) $(SHLINKNAME)

clean:
	-rm $(SHLIBNAME) $(SHLINKNAME) $(SRCS:.cc=.o)

$(SHLIBNAME): $(SRCS:.cc=.o)
	$(LINK.cc) $^ $(OUTPUT_OPTION)

$(SHLINKNAME): $(SHLIBNAME)
	-rm $@
	ln -s $< $@

install:
	mkdir -pv $(DESTDIR)/lib $(DESTDIR)/include/actor
	cp -v *.hpp $(DESTDIR)/include/actor/
	cp -v $(SHLINKNAME) $(SHLIBNAME) $(DESTDIR)/lib

uninstall:
	-rm -v $(DESTDIR)/lib/$(SHLINKNAME)
	-rm -v $(DESTDIR)/lib/$(SHLIBNAME)
