include ../mk/compile.mk
include ../mk/link.mk

all: $(PROG_CXX)

clean:
	- rm $(SRCS:.cc=.o) $(PROG_CXX)

$(PROG_CXX): $(SRCS:.cc=.o)
	$(LINK.cc) -o $@ $^ $(LDFLAGS)
