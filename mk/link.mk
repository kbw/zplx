include ../mk/platform.mk

LDFLAGS  += -L$(PREFIX)/lib -lzmq

ifeq ($(strip $(PLATFORM)), Linux)
  LDFLAGS += -ldl
endif
