
# CXXFLAGS += -std=c++17 -pedantic
# CPPFFAGS += -g -Wall -Wextra
CPPFLAGS += -I$(PREFIX)/include
LDFLAGS  += -L$(PREFIX)/lib -lzmqpp -lczmq

ifeq ($(strip $(TYPE)), shlib)
  ifeq ($(strip $(PLATFORM)), Darwin)
    LDFLAGS += -dynamiclib
  else
    CPPFLAGS += -fPIC
    LDFLAGS += -shared
  endif
endif

ifeq ($(strip $(PLATFORM)), Darwin)
  SHLIBNAME := lib$(SHLIB)-$(SHLIB_MAJOR).dylib
  SHLINKNAME:= lib$(SHLIB).dylib
else
  SHLIBNAME := lib$(SHLIB)-$(SHLIB_MAJOR).so
  SHLINKNAME:= lib$(SHLIB).so
endif

ifeq ($(strip $(PLATFORM)), Linux)
  LDFLAGS += -ldl
endif

.PHONY: all clean install uninstall
ifeq ($(strip $(TYPE)), shlib)
all: $(SHLIBNAME) $(SHLINKNAME)

clean:
	-rm $(SHLIBNAME) $(SHLINKNAME) $(SRCS:.cc=.o)

$(SHLIBNAME): $(SRCS:.cc=.o)
	$(LINK.cc) $^ $(OUTPUT_OPTION)

$(SHLINKNAME): $(SHLIBNAME)
	-rm $@
	ln -s $< $@

install:
	mkdir -pv $(DESTDIR)/lib $(DESTDIR)/include/actor
	cp -v *.hpp $(DESTDIR)/include/actor/
	cp -v $(SHLINKNAME) $(SHLIBNAME) $(DESTDIR)/lib

uninstall:
	-rm -v $(DESTDIR)/lib/$(SHLINKNAME)
	-rm -v $(DESTDIR)/lib/$(SHLIBNAME)
else
all: $(PROG_CXX)

clean:
	- rm $(SRCS:.cc=.o) $(PROG_CXX)

$(PROG_CXX): $(SRCS:.cc=.o)
	$(LINK.cc) -o $@ $^ $(LDFLAGS)
endif
