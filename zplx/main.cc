#include <unistd.h>
#include <zmq.hpp>
#include <libzplx/zplx.hh>

int main(int argc, char* argv[]) {
	zplx::entries_t entries{
		{STDIN_FILENO, ZMQ_POLLIN},
		{STDOUT_FILENO, ZMQ_POLLOUT}
	};
	zplx::process(entries);
}
